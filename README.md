### Single sector linux bootloader.

### How to use

Create MBR and add partitions for kernel.
```
fdisk /dev/sdX
```

Copy bootloader on the first block of the disk.
```
dd if=bootloader of=/dev/sdX bs=446 count=1
dd if=bootloader of=/dev/sdX bs=1 skip=510 seek=510 count=2
```

Copy kernel on the first and (optionally) second partition
```
dd if=bzImage of=/dev/sdX1
dd if=bzImage of=/dev/sdX2
```

By default bootloader will load kernel from the first partition.  
To load kernel from the second partition, you have to quickly
press `left shift` key multiple times during boot.
