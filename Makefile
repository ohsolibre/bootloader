all:
	as -o bootloader.o bootloader.s
	ld -o bootloader bootloader.o --oformat=binary -Ttext=0x7c00 

clean:
	rm -f bootloader bootloader.o
