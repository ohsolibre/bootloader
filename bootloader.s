.code16
.text
.globl _start
_start:
	cli
	xor %ax, %ax
	mov %ax, %ds
	mov %ax, %ss
	mov $0x7C00, %sp

	/* Enable A20 line */
	mov $0x2401, %ax
	int $0x15
	jc err

	/* Enable protected mode */
	lgdt gdt
	mov %cr0, %eax
	or $1, %eax
	mov %eax, %cr0
	jmp .+2

	/* Use first GDT descriptor for segments */
	mov $0x8, %bx
	mov %bx, %ds

	/* Disable protected mode */
	and $0xFE, %al
	mov %eax, %cr0

	/* Now we are in unreal mode */
	xor %ax, %ax
	mov %ax, %ds

	/* check if shift is pressed */
	xor %ebx, %ebx
	mov $0xffff, %cx
shift:
	mov $0x02, %ah
	int $0x16
	and $0x02, %al
	jne pressed
	loop shift

	mov (0x7C00 + 454), %ebx /* first partition */
	jmp do_read
pressed:
	mov (0x7C00 + 470), %ebx /* second partition */

do_read: /* load real-mode code to 0x10000 */
	mov %ebx, dap_sec
	mov dap_seg, %es
	xor %eax, %eax

	call read
	movb %es:(0x1f1), %al /* get size of real-mode code */
	addb $1, %al
	movb %al, dap_cnt
	call read
	add %eax, dap_sec
	
	cmpl $0x53726448, %es:(0x202) /* magic number */
	jne err
	movb $0xff, %es:(0x210)   /* loader type - undefined */
	movb $0x81, %es:(0x211)   /* flags - use heap, loaded high */
	movw $0xde00, %es:(0x224) /* heap_end_ptr */
	movw $cmdline, %es:(0x228)

	movl %es:(0x1f4), %ecx /* size of the kernel in units of 16-byte */
	shr $5, %ecx           /* convert to 512-byte sectors */
	inc %ecx               /* make sure we do not miss last sector */

	/* load 127 sectors at a time to 0x20000 then copy to 0x100000 */
	movw $127, dap_cnt
	movw $0x2000, dap_seg
	mov $0x100000, %edi
load:
	call read
	mov $127*512, %ax
	mov $0x20000, %esi
	call memcpy
	addl $127, dap_sec
	sub $127, %cx
	jg load

	mov $0xe000, %sp
	mov $0x1000, %ax
	mov %ax, %ds
	mov %ax, %es
	mov %ax, %fs
	mov %ax, %gs
	mov %ax, %ss
	jmp $0x1000, $0x200


read:
	mov $0x42, %ah
	mov $dap, %si
	int $0x13 /* %dl is set by bios to current drive */
	jc err
	ret

memcpy:
	mov (%esi), %ebp
	mov %ebp, (%edi)
	add $4, %esi
	add $4, %edi
	sub $4, %eax
	jnz memcpy
	ret

err:
	jmp $0xffff, $0 # reboot

cmdline: .asciz ""
dap: /* disk address packet */
	.byte 0x10 /* size - always 0x10 */
	.byte 0    /* unused */
dap_cnt:.word 1    /* number of sectors to read */
	.word 0    /* offset */
dap_seg:.word 0x1000 /* segment */
dap_sec:.quad 0    /* start sector number */

gdt:
	.word gdt_end - gdt_start - 1
	.int gdt_start
gdt_start:
	.quad 0      /* first empty entry */

	.word 0xFFFF /* limit [0:15] */
	.word 0      /* base  [0:15] */
	.byte 0      /* base  [16:23] */
	.byte 0x92   /* access - Data/RW */
	.byte 0xCF   /* flags - 4K/32bit; limit [16:19] */
	.byte 0      /* base  [24:31] */
gdt_end:
	
. = _start + 510
.word 0xaa55
